import { Component, OnInit, Input } from '@angular/core';
import { LabelService } from './label.service';
import * as Gitlab from 'src/app/types';
import { Observable } from 'rxjs';

@Component({
  selector: 'gdb-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss']
})
export class LabelComponent implements OnInit {
  @Input()
  name: string;

  labelData$: Observable<Gitlab.ProjectLabel>;

  constructor(
    private labelService: LabelService
  ) { }

  ngOnInit() {
    this.labelData$ = this.labelService.getLabelByName(this.name);
  }

}
