import { Injectable } from '@angular/core';
import { GitlabApiService } from 'src/app/core/gitlab-api.service';
import * as Gitlab from 'src/app/types';
import { first, filter, map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

interface LabelsMap {
  [key: string]: Gitlab.ProjectLabel
}

@Injectable({
  providedIn: 'root'
})
export class LabelService {
  private labels: LabelsMap;
  public ready$: BehaviorSubject<boolean>;

  constructor(
    private gitlabApiService: GitlabApiService
  ) {
    this.labels = {};
    this.ready$ = new BehaviorSubject(false);
  }

  init() {
    this.gitlabApiService
      .getLabels('10499429')
      .pipe(first())
      .subscribe(labels => {
        this.labels = labels.reduce(
          (accum, curr) => {
            accum[curr.name] = curr;
            return accum;
          },
          { } as LabelsMap
        );

        this.ready$.next(true);
        this.ready$.complete();
      })
  }

  getLabelByName(name: Gitlab.ProjectLabel['name']) {
    return this.ready$.pipe(
      filter(isReady => isReady),
      map(() => this.labels[name])
    );
  }

}
