import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LabelComponent } from './label/label.component';
import { DateAgoPipe } from './pipes/date-ago.pipe';

@NgModule({
  declarations: [ LabelComponent, DateAgoPipe ],
  exports: [ LabelComponent, DateAgoPipe ],
  imports: [
    CommonModule
  ]
})
export class AppCommonsModule { }
