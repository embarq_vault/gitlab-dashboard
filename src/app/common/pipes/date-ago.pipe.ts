import { Pipe, PipeTransform } from '@angular/core';
import { formatDistanceToNow } from 'date-fns';

@Pipe({
  name: 'dateAgo',
  pure: true
})
export class DateAgoPipe implements PipeTransform {

  transform(value: any): string {
    const dateValue = new Date(value);
    if (Number.isNaN(dateValue as any)) {
      console.warn(`DateAgoPipe: value "${ value }" seems to be invalid date`);
      return value;
    }

    return formatDistanceToNow(dateValue);
  }

}
