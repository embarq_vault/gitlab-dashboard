import { Component } from '@angular/core';

@Component({
  selector: 'gdb-root',
  template: `
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {}
