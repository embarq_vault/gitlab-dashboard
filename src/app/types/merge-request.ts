export interface MergeRequest {
  id: number;
  iid: number;
  project_id: number;
  title: string;
  description: string;
  state: string;
  created_at: string;
  updated_at: string;
  target_branch: string;
  source_branch: string;
  upvotes: number;
  downvotes: number;
  author: Author;
  user: User;
  assignee: Author;
  assignees: Assignee[];
  source_project_id: number;
  target_project_id: number;
  labels: string[];
  work_in_progress: boolean;
  milestone: Milestone;
  merge_when_pipeline_succeeds: boolean;
  merge_status: string;
  merge_error?: any;
  sha: string;
  merge_commit_sha?: any;
  squash_commit_sha?: any;
  user_notes_count: number;
  discussion_locked?: any;
  should_remove_source_branch: boolean;
  force_remove_source_branch: boolean;
  allow_collaboration: boolean;
  allow_maintainer_to_push: boolean;
  web_url: string;
  time_stats: Timestats;
  squash: boolean;
  subscribed: boolean;
  changes_count: string;
  merged_by: Assignee;
  merged_at: string;
  closed_by?: any;
  closed_at?: any;
  latest_build_started_at: string;
  latest_build_finished_at: string;
  first_deployed_to_production_at?: any;
  pipeline: Pipeline;
  diff_refs: Diffrefs;
  diverged_commits_count: number;
  rebase_in_progress: boolean;
  task_completion_status: TaskCompletionStatus;
  has_conflicts: boolean;
  blocking_discussions_resolved: boolean;
}

interface TaskCompletionStatus {
  count: number;
  completed_count: number;
}

interface Diffrefs {
  base_sha: string;
  head_sha: string;
  start_sha: string;
}

interface Pipeline {
  id: number;
  sha: string;
  ref: string;
  status: string;
  web_url: string;
}

interface Timestats {
  time_estimate: number;
  total_time_spent: number;
  human_time_estimate?: any;
  human_total_time_spent?: any;
}

interface Milestone {
  id: number;
  iid: number;
  project_id: number;
  title: string;
  description: string;
  state: string;
  created_at: string;
  updated_at: string;
  due_date: string;
  start_date: string;
  web_url: string;
}

interface Assignee {
  name: string;
  username: string;
  id: number;
  state: string;
  avatar_url: string;
  web_url: string;
}

interface User {
  can_merge: boolean;
}

interface Author {
  id: number;
  name: string;
  username: string;
  state: string;
  avatar_url?: any;
  web_url: string;
}