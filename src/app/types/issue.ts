import { MergeRequest } from './merge-request';

export interface Issue {
  id: number;
  iid: number;
  project_id: number;
  title: string;
  description: string;
  state: string;
  created_at: string;
  updated_at: string;
  closed_at?: any;
  closed_by?: any;
  labels: string[];
  milestone?: any;
  assignees: Assignee[];
  author: Assignee;
  assignee: Assignee;
  user_notes_count: number;
  merge_requests_count: number;
  upvotes: number;
  downvotes: number;
  due_date?: any;
  confidential: boolean;
  discussion_locked?: any;
  web_url: string;
  time_stats: Timestats;
  task_completion_status: TaskCompletionStatus;
  has_tasks: boolean;
  task_status: string;
  _links: Links;
  moved_to_id?: any;
  merge_requests?: MergeRequest[];
}

interface Links {
  self: string;
  notes: string;
  award_emoji: string;
  project: string;
}

interface TaskCompletionStatus {
  count: number;
  completed_count: number;
}

interface Timestats {
  time_estimate: number;
  total_time_spent: number;
  human_time_estimate?: any;
  human_total_time_spent?: any;
}

interface Assignee {
  id: number;
  name: string;
  username: string;
  state: string;
  avatar_url: string;
  web_url: string;
}

type IssueRequestOptionsOrderBy = 'created_at' | 'updated_at' | 'priority' | 'due_date' | 'relative_position' | 'label_priority' | 'milestone_due' | 'popularity' | 'weight'

export interface IssueRequestOptions {
  /** @default 'all' */
  state?: 'all' | 'opened' | 'closed';
  /** Comma-separated list of label names, issues must have all labels to be returned */
  labels?: string;
  with_labels_details?: boolean;
  /**
   * Return issues for the given scope: `created_by_me`, `assigned_to_me` or `all`
   * @default 'created_by_me'
   */
  scope?: 'created_by_me' | 'assigned_to_me' | 'all';
  author_username?: string;
  assignee_username?: string;
  order_by?: IssueRequestOptionsOrderBy;
  /** @default 'desc' */
  sort?: 'asc' | 'desc';
  created_after?: string;
  created_before?: string;
  updated_after?: string;
  updated_before?: string;
}