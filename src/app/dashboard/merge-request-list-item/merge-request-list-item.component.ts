import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import * as Gitlab from 'src/app/types';

@Component({
  selector: 'gdb-merge-request-list-item',
  templateUrl: './merge-request-list-item.component.html',
  styleUrls: ['./merge-request-list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MergeRequestListItemComponent implements OnInit {
  @Input() 
  public data: Gitlab.MergeRequest;

  constructor() { }

  ngOnInit() {
  }

}
