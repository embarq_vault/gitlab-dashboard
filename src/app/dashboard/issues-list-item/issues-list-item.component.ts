import { Component, OnInit, Input } from '@angular/core';
import * as Gitlab from 'src/app/types';

@Component({
  selector: 'gdb-issues-list-item',
  templateUrl: './issues-list-item.component.html',
  styleUrls: ['./issues-list-item.component.scss']
})
export class IssuesListItemComponent implements OnInit {
  @Input()
  data: Gitlab.Issue;

  constructor() { }

  ngOnInit() {
  }

}
