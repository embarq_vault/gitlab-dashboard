import { Component, OnInit } from '@angular/core';
import { LabelService } from '../common/label/label.service';

@Component({
  selector: 'gdb-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private labelService: LabelService) {
    this.labelService.init();
  }

  ngOnInit() {
  }

}
