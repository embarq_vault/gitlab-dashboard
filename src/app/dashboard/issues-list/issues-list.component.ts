import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import * as Gitlab from 'src/app/types';

@Component({
  selector: 'gdb-issues-list',
  templateUrl: './issues-list.component.html',
  styleUrls: ['./issues-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssuesListComponent implements OnInit {
  @Input()
  public data: Gitlab.Issue[];

  constructor() { }

  ngOnInit() {
  }

}
