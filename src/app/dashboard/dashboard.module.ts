import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard.component';
import { IssuesListComponent } from './issues-list/issues-list.component';
import { IssuesListItemComponent } from './issues-list-item/issues-list-item.component';
import { MergeRequestListComponent } from './merge-request-list/merge-request-list.component';
import { MergeRequestListItemComponent } from './merge-request-list-item/merge-request-list-item.component';
import { AppCommonsModule } from '../common/common.module';
import { DashboardRouingModule } from './dashboard-routing.module';
import { IssueListPageComponent } from './issue-list-page/issue-list-page.component';

@NgModule({
  declarations: [
    DashboardComponent,
    IssuesListComponent,
    IssuesListItemComponent,
    MergeRequestListComponent,
    MergeRequestListItemComponent,
    IssueListPageComponent
  ],
  entryComponents: [ IssueListPageComponent ],
  imports: [
    CommonModule,
    DashboardRouingModule,
    AppCommonsModule
  ]
})
export class DashboardModule { }
