import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { GitlabApiService } from 'src/app/core/gitlab-api.service';
import * as Gitlab from 'src/app/types';
import { Observable, merge, zip, of } from 'rxjs';
import { mergeMap, concatMap, map } from 'rxjs/operators';
import { gitlabApiStub } from 'src/app/core/gitlab-api-stub';

@Component({
  selector: 'gdb-issue-list-page',
  templateUrl: './issue-list-page.component.html',
  styleUrls: ['./issue-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueListPageComponent implements OnInit {
  public issues$: Observable<Gitlab.Issue[]>;

  constructor(
    private gitlabApiService: GitlabApiService
  ) { }

  ngOnInit() {
    // this.issues$ = this.gitlabApiService.getIssues('10499429').pipe(
    //   concatMap((issues) => {
    //     const extendedIssues$ = issues.map(issue => 
    //       this.gitlabApiService
    //         .getIssueMergeRequests(issue.project_id as any, issue.iid as any)
    //         .pipe(
    //           map(merge_requests => {
    //             return {
    //               ...issue,
    //               merge_requests
    //             }
    //           })
    //         )
    //     );
    //     return zip(...extendedIssues$)
    //   })
    // );
    this.issues$ = of(gitlabApiStub as any);
    this.issues$.subscribe(data => console.log(data));
  }

}
