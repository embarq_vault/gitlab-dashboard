import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import * as Gitlab from 'src/app/types';

@Component({
  selector: 'gdb-merge-request-list',
  templateUrl: './merge-request-list.component.html',
  styleUrls: ['./merge-request-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MergeRequestListComponent implements OnInit {
  @Input()
  public data: Gitlab.MergeRequest[];

  constructor() { }

  ngOnInit() {
  }

}
