import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { GitlabApiService } from './gitlab-api.service';
import { GitlabApiInterceptorService } from './gitlab-api-interceptor.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    GitlabApiService,
    { provide: HTTP_INTERCEPTORS, useClass: GitlabApiInterceptorService, multi: true }
  ]
})
export class CoreModule { }
