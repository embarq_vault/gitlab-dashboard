import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import * as Gitlab from '../types';
import { zip, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

function api(path: string) {
  return `https://gitlab.com/api/v4${path}`;
}

@Injectable({
  providedIn: 'root'
})
export class GitlabApiService {
  constructor(
    private http: HttpClient
  ) { }

  getIssues(projectId: string, options: Gitlab.IssueRequestOptions = {}) {
    const params = new HttpParams({
      fromObject: options as any
    });
    return this.http.get<Gitlab.Issue[]>(
      api(`/projects/${ projectId }/issues`),
      { params }
    );
  }

  getSingleIssue(
    projectId: string,
    id: string,
    options: Gitlab.IssueRequestOptions = {}
  ): Observable<Gitlab.Issue> {
    const issueReqParams = new HttpParams({
      fromObject: options as any
    });
    const issue$ = this.http.get<Gitlab.Issue>(
      api(`/projects/${ projectId }/issues/${ id }`),
      { params: issueReqParams }
    );
    const relatedMRs$ = this.getIssueMergeRequests(projectId, id);
    return zip(issue$, relatedMRs$).pipe(
      map(([issue, merge_requests]) => {
        return {
          ...issue,
          merge_requests
        }
      })
    )
  }

  getIssueMergeRequests(projectId: string, id: string) {
    return this.http.get<Gitlab.MergeRequest[]>(
      api(`/projects/${ projectId }/issues/${ id }/related_merge_requests`)
    );
  }

  getSingleMergeRequest(projectId: string, id: string) {
    return this.http.get<Gitlab.MergeRequest>(api(`/projects/${ projectId }/merge_requests/${ id }`));
  }

  getLabels(projectId: string) {
    return this.http.get<Gitlab.ProjectLabel[]>(api(`/projects/${ projectId }/labels?per_page=100`));
  }

  getProjects(username: string) {
    return this.http.get<Gitlab.Project[]>(api(`/users/${ username }/projects`));
  }
}
