import { gitlab_token } from '../../.runtimeconfig.json';

export const environment = {
  production: true,
  gitlab_token
};
